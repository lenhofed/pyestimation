<div align="center">

# pyEstimation
</div>

## Content

1. [Purpose](#purpose)

2. [Requirements](#requirements)

3. [Installation](#installation)

4. [Dependencies](#dependencies)

5. [Documentation](#documentation)

6. [License](#license)

7. [Notes](#notes)

## Purpose

pyEDstimation is a Python toolbox originally developped by ENSG students.

It is designed for educational purpose on numerical estimation methods. Therefore, it is focused on simplicity, lisibility and adaptability over efficiency.

## Requirements

You need Python 3.6 or later to be able to use `pyEDstimation` (it has no warranty to work on any other version). You can have multiple Python versions (2.x and 3.x) installed on the same operating system without any problem.

## Installation

First, you have to clone `pyEstimation` package with git.

`pyEstimation` package can be installed on windows, linux or mac os.

- Using setuptool:

```shell
$ cd pathtorepertorypyEstimation
$ python setup.py install
```

- Using pip:

```shell
$ cd pathtorepertorypyEstimation
$ pip install -U .
```

With pip, all the necessary packages are automatically installed.

## Dependencies

The `pyEstimation` package depends on two Python packages :

- scipy
- matplotlib

## Documentation

Sources for **Sphinx** documentation are avalaible. You can build the *html* documentation simply by running the command `make html` in the **pyestimation/doc** directory.
This will output the *html* documentation in the **pyestimation/doc/build** directory

## License

pyEDstimation is distributed as a free software in order to build a community of users, contributors,
developers who will contribute to the project and ensure the necessary means for its evolution.
pyEDstimation is a free software; you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version. Any modification of source code in this
LGPL software must also be published under the LGPL license.

pyEDstimation is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Please feel free to contribute to the project directly on the GitLab repository

## Notes 

You can find an example.py script that briefly presents how to use pyEDstimation. It is located in the **pyestimation/example** directory.

