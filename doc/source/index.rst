.. pyEDstimation documentation master file, created by
   sphinx-quickstart on Sat Mar 14 18:52:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyEDstimation's documentation!
=========================================

   pyEDstimation is a Python toolbox originally developped by ENSG students. \
   It is designed for educational purpose on numerical estimation methods. 
   Therefore, it is focused on simplicity, lisibility and adaptability over efficiency.
  
   pyEDstimation is distributed as a free software in order to build a community of users, contributors,
   developers who will contribute to the project and ensure the necessary means for its evolution.

   pyEDstimation is a free software; you can redistribute it and/or modify it under the terms of the
   GNU Lesser General Public License as published by the Free Software Foundation; either version 3
   of the License, or (at your option) any later version. Any modification of source code in this
   LGPL software must also be published under the LGPL license.

   pyEDstimation is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 
   Please feel free to contribute to the project directly on the `GitLab repository`_
 
   
   .. _GitLab repository: https://gitlab.com/lenhofed/pyestimation

.. toctree::
   :maxdepth: 2
   :caption: Contents :
   
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
