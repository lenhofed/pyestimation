pyestimation
============

.. toctree::
   :maxdepth: 4

   constants
   estimationMethods
   model
   utils
