#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 10:01:18 2020

@author: edgar

This script is intended to give a first idea on how to use the pyEDstimation toolbox
"""

# Imports
import math as m
from random import gauss
import matplotlib.pyplot as plt
from pyestimation.estimationMethods.least_squares import LeastSquares, Ransac
from pyestimation.model import Sinus, Linear, Parameter
from pyestimation import utils

# get_ipython().run_line_magic('matplotlib', 'inline')


# First let's create a fake set of observation
# It will follow a simple combination of sinus and linear functions and a white gaussian noise
def obs(x):
    return (4.3659 * x + 10.63249) + (13.2943 * m.sin(0.96186 * x + 3.45455)) + gauss(0, 3)


t = [i * 0.1 for i in range(500)]
l = [obs(t[i]) for i in range(len(t))]

# Let's have a look at it
plt.figure(1)
plt.plot(t,l)
plt.title('The raw observations')
plt.show()

# Let's define the model to be estimated

# We can clearly identify a trend so let's define a linear model with approximated parameters
# You can list available parameters such as :
utils.printAvalaibleParameterType()
a = Parameter('linearCOEFF', 6)  # linear coefficient
b = Parameter('linearORIGIN', 15)  # value at origin

# You can list available models such as :
utils.printAvalaibleParameterType()
linear = Linear(coefficient=a, origin=b)
# We also identify a sinusoidal model with approximated parameters
A = Parameter("sinA", 10)  # amplitude of the sinus
w = Parameter("sinW", 1)  # pulsation of the sinus
phi = Parameter("sinPHI", m.pi)  # phase shift of the sinus
sin = Sinus(w, amplitude=A, phase_shift=phi)

# the model is a simple addition of the two base Function
model = linear + sin

# Let's see what our initial model looks like compared to the raw observations
plt.figure(2)
plt.plot(t, l)
plt.plot(t, [model.function(t[i]) for i in range(len(t))])
plt.title('The approximate model compared with the raw observations')
plt.show()

# Now let's make a better estimation using least squares method
MC = LeastSquares(t, l, model, sigmas_2=[3 ** 2 for i in range(len(l))])
MC.estimate(treshold=0.01, iteration_max=7)  # the model is non linear so we use iterative LSE
# And plot the result of the estimation
plt.figure(3)
plt.plot(t, l)
plt.plot(t, [MC.model.function(t[i]) for i in range(len(t))])
plt.title('The estimated LSE model compared with the raw observations')
plt.show()
# We can display a summary of the estimation
MC.summarize()



t2 = [i*10 for i in range(50)]
# Now let's try to estimate a model with outliers
def obs2(x):
    return (4.3659 * x + 10.63249) + gauss(0, 3)

l2 = [obs2(t2[i]) for i in range(len(t2))]
#let's add outliers
l2[0] = -10
l2[5] = -10
l2[10] = -10
l2[15] = -10
l2[20] = -10
l2[25] = -10
l2[30] = -10
l2[35] = -10




# the model is a simple addition of the two base Function
model2 = Linear(coefficient=a, origin=b)

# Let's see what our initial model looks like compared to the raw observations
plt.figure(4)
plt.plot(t2, l2)
plt.plot(t2, [model2.function(t2[i]) for i in range(len(t2))])
plt.title('The approximate model compared with the raw observations')
plt.show()

# Let's try anr estimation using basic least squares method
MC2 = LeastSquares(t2, l2, model2, sigmas_2=[3 ** 2 for i in range(len(l2))])
MC2.estimate(iteration_max=1)  # the model is linear so we use normal LSE
# And plot the result of the estimation
plt.figure(5)
plt.plot(t2, l2)
plt.plot(t2, [MC2.model.function(t2[i]) for i in range(len(t2))])
plt.title('The estimated LSE model compared with the raw observations')
plt.show()

# We can use the automatic detection of outliers 
MC3 = LeastSquares(t2, l2, model2, sigmas_2=[3 ** 2 for i in range(len(l2))])
MC3.estimate(iteration_max=1, outliers_autodetect=True)  # the model is linear so we use normal LSE
# And plot the result of the estimation
plt.figure(6)
plt.plot(t2, l2)
plt.plot(t2, [MC3.model.function(t2[i]) for i in range(len(t2))])
plt.plot([MC3.outliers[i][0] for i in range(len(MC3.outliers))], [MC3.outliers[i][1] for i in range(len(MC3.outliers))], 'ro')
plt.title('The estimated LSE model compared with the raw observations with outliers detection')
plt.show() 

# Or use the RANSAC method
RANSAC = Ransac(t2, l2, model2)
RANSAC.estimate(3, consensus_size_treshold=len(t2)*0.99, iteration_max=10, LSE_iteration_max=1)
plt.figure(7)
plt.plot(t2,l2)
plt.plot(t2, [RANSAC.model.function(t2[i]) for i in range(len(t2))])
plt.title('The estimated RANSAC model compared with the raw observations')
plt.show()
RANSAC.summarize()