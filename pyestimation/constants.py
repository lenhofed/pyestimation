#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:mod:`constants` 
=====================

This module contains the constant attributs of the toolbox.
They should be modified with care if needed.

PARAMETER_TYPES
****************
Allowed parameter types in model functions.
Can be expanded.

MODEL_TYPES
****************
Implemented model functions.
Can be expanded.

:Authors:
    Edgar LENHOF

:Version: 1.0 of 2020/03/06
"""

PARAMETER_TYPES = ['sinA', 'sinW', 'sinPHI', 'linearCOEFF', 'linearORIGIN', 'polynomCOEFF',
                   'exponentialIN', 'exponentialOUT']
MODEL_TYPES = ['Sinus', 'Cosinus', 'Linear', 'Polynom', 'Exponential']