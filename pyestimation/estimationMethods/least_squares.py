#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:mod:`least_squares`
=======================
This module contains the classes implementing least squares method

:Authors:
    Edgar LENHOF

:Version: 1.0 of 2020/03/07

Class overview
--------------

.. autosummary::
    
    LeastSquares
    
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import chi2
from scipy.stats import t as student
from scipy.stats import f as fisher
import copy


class LeastSquares():
    """
    Class LeastSquares implements the least squares estimation of a model to fit observations

    :param t: list abscisse values of observations
    :type t: float[]
    :param l: list of observations, should have same size as t
    :type l: float[]
    :param model: the model to be estimated from observations, setted at first with approx parameters
    :type model: Function
    :param SigmaL: the normalized matrice of variance/covariance of observations, can also be constructed with the following parameters, defaults to None
    :type SigmaL: float array of size=nb_obs * nb_obs, optional
    :param sigmas2: a list containing the variances of each observation, defaults to None
    :type sigmas_2: float[], optional
    :param sigma0_2: the reference variance value of observations, defaults to None
    :type sigma0_2: float, optional
    :param M: the cofactors matrice that will propagate variance, defaults to None
    :type M: float array of size=nb_relations_between_obs * nb_obs, optional
    """

    def __init__(self, t, l, model, *args, SigmaL=None, sigmas_2=None, sigma0_2=None, M=None, **kwargs):
        """
        Constructor method
        """
        self.l = copy.deepcopy(l)  # observation
        self.t = copy.deepcopy(t)  # abscisse values
        self.model = model  # model of the estimation

        # set LSE matrices
        self.setX0()
        self.setA()
        self.setB()
        self.setQL(SigmaL=SigmaL, sigmas_2=sigmas_2, sigma0_2=sigma0_2, M=M)
        self.P = np.linalg.pinv(self.QL)

        self.initX0 = copy.deepcopy(self.X0) # X0 is going to be modified on iterative estimation
        
    def estimate(self, *args, treshold=0, iteration_max=1, outliers_autodetect=False, **kwargs):
        """
        Base method to be used for Least Squares Estimation. 
        If called with no arguments it will run a simple 1 iteration estimation.
        
        :param treshold: the treshold value where the difference between two steps is accepted, defaults to 0
        :type treshold: float, optional
        :param iteration_max: the maximum number of iterations to do in case of non-convergence or to avoid to much computing, defaults to 1
        :type iteration_max: int, optional
        :param outliers_autodetect: allows the automatic detection and suppression of outliers, defaults to False
        :type outliers_autodetect: boolean, optional
        """
        self.outliers = []
        # first run
        self.computeIterativeLSE(treshold, iteration_max)
        self.compensationQualification()       
        
        if outliers_autodetect :
            
            test, indice_maximum, maximum, stud = self.testNormalizedResidual()
            # if any outlier detected
            while test == False :
                self.outliers.append((self.t.pop(indice_maximum), self.l.pop(indice_maximum)))
                self.A = np.delete(self.A, indice_maximum, 0)
                self.B = np.delete(self.B, indice_maximum, 0)
                
                self.QL = np.delete(self.QL, indice_maximum, 0)
                self.QL = np.delete(self.QL, indice_maximum, 1)
                
                self.P = np.delete(self.P, indice_maximum, 0)
                self.P = np.delete(self.P, indice_maximum, 1)
                
                self.computeIterativeLSE(treshold, iteration_max)
                self.compensationQualification()       
                test, indice_maximum, maximum, stud = self.testNormalizedResidual()

    def computeLSE(self):
        """
        Computes the compensed parameters using A, B and P matrices
        """
        self.N = (self.A.T) * self.P * self.A  # matrice normal
        self.dX_comp = np.linalg.pinv(self.N) * ((self.A.T) * self.P * self.B)
        self.X_comp = self.X0 + self.dX_comp  # compensed parameters

    def computeIterativeLSE(self, treshold, iteration_max, *args, **kwargs):
        """
        Computes the compensed parameters using A, B and P matrices with multiple iterations
        Necessary for non-linear model convergence

        :param treshold: the treshold value where the difference between two steps is accepted
        :type treshold: float
        :param iteration_max: the maximum number of iterations to do in case of non-convergence or to avoid to much computing
        :type iteration_max: int
        """
        self.k = 0  # iteration counter
        self.X_comp = self.X0 + (treshold + 1) * 1000  # initializes X_comp to enter while loop

        # as soon as the model converges under the treshol value, estimation stops
        while np.max(np.abs(self.X_comp - self.X0)) > treshold and self.k <= iteration_max:
            if self.k != 0:  # we recompute matrices with the new parameters
                self.setA()
                self.setB()
                self.setX0()

            self.computeLSE()
            self.changeApproxParameters()

            self.k += 1

    def setX0(self):
        """
        Sets the approximate parameters vector from model parameters
        size=nb_parameters
        """
        self.X0 = np.asmatrix(np.zeros((len(self.model.parameters), 1)))
        for i in range(len(self.model.parameters)):
            self.X0[i, 0] = self.model.parameters[i].value

    def setA(self):
        """
        Sets the model matrix A using the predefined model
        size=nb_obs * nb_parameters
        """
        self.A = np.asmatrix(np.zeros((len(self.t), len(self.model.parameters))))

        for i in range(len(self.t)):
            for k in range(len(self.model.parameters)):
                self.A[i, k] = self.model.partialDerivative(self.t[i], self.model.parameters[k])

    def setB(self):
        """
        Sets the closure matrix B using the predefined model
        size=nb_obs
        """
        self.B = np.asmatrix(np.zeros((len(self.l), 1)))
        for i in range(len(self.l)):
            self.B[i,0] = self.l[i] - self.model.function(self.t[i])
            
    def setQL(self, *args, SigmaL=None, sigmas_2=None, sigma0_2=None, M=None, **kwargs):
        """
        Sets the matrice of normalized variance/covariance of observations.
        If no argument is passed, the matrice will be set to Id matrice

        :param SigmaL: the matrice of variance/covariance of observations, can also be constructed with the following parameters, defaults to None
        :type SigmaL: float array of size=nb_obs * nb_obs, optional
        :param sigmas2: a list containing the variances of each observation, defaults to None
        :type sigmas_2: float[], optional
        :param sigma0_2: the reference variance value of observations, defaults to None
        :type sigma0_2: float, optional
        :param M: the cofactors matrice that will propagate variance in case of corrolation, defaults to None
        :type M: float array of size=nb_relations_between_obs * nb_obs, optional
        """
        if SigmaL != None:  # setting SigmaL from a matrix
            self.SigmaL = np.asmatrix(SigmaL)

        else:
            self.SigmaL = np.asmatrix(np.eye(len(self.l)))

            if sigmas_2 != None:  # setting SigmaL from sigmas of observation
                for i in range(len(sigmas_2)):
                    self.SigmaL[i, i] = sigmas_2[i]
                if M != None:  # if sigmas are corrolated
                    M = np.matrix(M)
                    self.SigmaL = M * self.SigmaL * M.T

        if sigma0_2 == None:  # we take the mean value of all sigmas
            self.sigma0_2 = np.mean(np.diagonal(self.SigmaL))

        else: #we take input value
            self.sigma0_2 = sigma0_2  
            
        self.QL = self.SigmaL/self.sigma0_2 #normalization            
            
    def compensationQualification(self):
        """
        Sets the different qualificators of the compensation : 
            Qx_comp : normalized matrice of variance/covariance of estimated parameters \n
            V_comp : vector of compensation residuals \n
            QV_comp : normalized matrice of variance/covariance of compensation residuals \n
            QL_comp : normalized matrice of variance/covariance of compensed observations \n
            EQM : mean quadratic error AKA sigma0 comp
        """
        self.QX_comp = np.linalg.pinv(self.N) # normalized matrice of variance/covariance of estimated parameters
        self.V_comp = self.B - self.A * self.dX_comp # vector of compensation residuals
        self.QV_comp = self.QL - self.A*self.QX_comp*(self.A.T) # normalized matrice of variance/covariance of compensation residuals
        self.QL_comp = self.QL - self.QV_comp # normalized matrice of variance/covariance of compensed observations
        self.EQM = ((self.V_comp.T)*self.P*self.V_comp)/(self.B.shape[0] - self.X0.shape[0]) # mean quadratic error AKA sigma0 comp

    def changeApproxParameters(self):
        """
        Changes the value of the parameters of the model with the compensed ones obtained after least squares estimation
        """
        for i in range(len(self.model.parameters)):
            self.model.parameters[i].setValue(self.X_comp[i, 0])

    def summarize(self):
        """
        Prints a summary of the least squares estimation
        """
        print("******************************************")
        print("******** LEAST SQUARES ESTIMATION ********")
        print("***************  SUMMARY  ****************")
        print("******************************************")
        print()
        
        print('Model used : ',self.model.name)
        print('Number of iterations : ', self.k + 1)
        print('Number of outliers automatically detected : ', len(self.outliers))
        print('\n')

        print("   ####### Compensed Parameters #######   ")
        for param in self.model.parameters:
            print('----------------------')
            print(param)
        print('\n')
        
        print("  ####### Statistics on Residuals #######  ")
        print('Mean value : ', np.mean(self.V_comp))
        print('Max value : ', np.max(self.V_comp))
        print('Min value : ', np.min(self.V_comp))
        self.plotResidualsHistogram()
        print('\n')
        
        print("  ###### Statistics on Compensation ######  ")
        testNR, indice, value, stud = self.testNormalizedResidual()
        print('Test on normalized residuals passed (95%) : ', testNR)
        print('Greatest value is at position ', indice)
        print('Value = ',value, ' < ', stud) 
        print()
        
        ell_axes = self.ellipsoidOfConfidence()
        for k in range(len(ell_axes)):
            print('Demi grand axe of ellipsoid of confidence for parameter ',ell_axes[k][0].name,' ',ell_axes[k][0].type, ' (95%) : ', ell_axes[k][1])
        print()
        
        testPARAM, stud = self.testParametersSignificancy()
        for k in range(len(testPARAM)):            
            print('Significancy test passed for ',testPARAM[k][0].name,' ',testPARAM[k][0].type, ' (95%) : ', testPARAM[k][1])
            print('Value = ',testPARAM[k][2], ' > ', stud) 
        print()
        
        testCHI2, left, x, right = self.testChi2()
        print("Test Chi2 passed (95%) : ", testCHI2)
        print(left," < sig0_comp/sig0 = ",x, " < ",right)


    def plotResidualsHistogram(self):
        """
        Plots the histogram of residuals
        """
        plt.figure()
        plt.hist(self.V_comp)
        plt.title('Histogram of the compensed residuals')
        plt.show()

    def testChi2(self, alpha=0.05):
        """
        Determines if the test of the Chi2 is passed

        :param alpha: the value of the quantil of our interval of confidence, defaults to 0.05
        :type alpha: float, optional

        :return: the result of the test
        :rtype: boolean
        :return:the left tolerance boundary
        :rtype: float    
        :return:the value of sig0_comp/sig0
        :rtype: float     
        :return: the right tolerance boundary
        :rtype: float 
        """
        df = self.B.shape[0] - self.X0.shape[0]
        left = np.sqrt(chi2.ppf(alpha / 2, df) / df)
        right = np.sqrt(chi2.ppf(1 - alpha / 2, df) / df)
        x = np.sqrt(self.EQM[0, 0] / self.sigma0_2)

        if left < x and x < right:
            test = True
        else:
            test = False
        return test, left, x, right

    
    def testNormalizedResidual(self,alpha=0.05):
        """
        Determines if the test on normalized residuals is passed
        
        :param alpha: the value of the quantil of our interval of confidence, defaults to 0.05
        :type alpha: float, optional
        
        :return: the result of the test
        :rtype: boolean 
        :return: the maximum normalized residuals of the compensation index
        :rtype: array_index[]   
        :return: value of maximum normalized residuals of the compensation
        :rtype: float   
        :return: the limit value of the student test
        :rtype: float
        """
        testResiduals = np.abs(copy.deepcopy(self.V_comp))
        for i in range(testResiduals.shape[0]):
            testResiduals[i,0] = testResiduals[i,0]/np.sqrt(self.EQM * self.QV_comp[i,i])

        df = self.B.shape[0] - self.X0.shape[0]
        stud = 2*student.ppf(1-alpha/2, df) # 2* because we test on the entire law student intervall of confidence
        maximum = np.max(testResiduals)
        indice = np.where( testResiduals == maximum)
        
        if maximum <= stud:
            test = True
        else:
            test = False
            
        return test, indice[0][0], maximum, stud
    
    def testParametersSignificancy(self,alpha=0.05):
        """
        Determines parameters significancy
        
        :param alpha: the value of the quantil of our interval of confidence, defaults to 0.05
        :type alpha: float, optional
        
        :return: a list containing the result and value of significancy test for each parameters and the limit value of the student test
        :rtype: [Parameter, boolean, float][], float
        :return: the value of the student test not to exceed
        :rtype: float
        """
        results = []
        testParameters = np.abs(self.X_comp-self.initX0)
        
        df = self.B.shape[0] - self.X0.shape[0]
        stud = 2*student.ppf(1-alpha/2, df) # 2* because we test on the entire law student intervall of confidence
        
        for i in range(testParameters.shape[0]):
            value = testParameters[i,0]/np.sqrt(self.EQM * self.QX_comp[i,i])
            value = value[0,0]
            if value >= stud:
                test = True
            else:
                test = False
            results.append([self.model.parameters[i], test, value])
                
        return results, stud
    
    def ellipsoidOfConfidence(self, alpha=0.05):
        """
        Determines the demi grand axes of the confidence ellipsoid.
        Ellispoid has dimension = number of parameters
        
        :param alpha: the value of the quantil of our interval of confidence, defaults to 0.05
        :type alpha: float, optional
        
        :return: a list containing the values of demi grand axes for each parameters in the ellipsoid of confidence
        :rtype: float[]
        """
        demi_grand_axes = []        
        eigenValues = np.linalg.eigvals(self.QX_comp)
        
        df1 = self.X_comp.shape[0]
        df2 = self.B.shape[0] - self.X0.shape[0]      
        fish = fisher.ppf(1-alpha, df1, df2)
        
        for i in range(eigenValues.size):
            dga = np.sqrt(self.EQM * 2 * eigenValues[i] * fish)[0,0]
            demi_grand_axes.append((self.model.parameters[i], dga))
        
        return demi_grand_axes
        
