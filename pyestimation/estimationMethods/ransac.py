# -*- coding: utf-8 -*-
"""
:mod:`ransac`
=======================
This module contains the classes implementing ransac method

:Authors:
    Edgar LENHOF

:Version: 1.0 of 2020/03/07

Class overview
--------------

.. autosummary::

    Ransac

"""
from random import randint
from pyestimation.estimationMethods.least_squares import LeastSquares

class Ransac():
    """
    Class Ransac implements the RANSAC estimation of a model to fit observations.
    The RANSAC method estimates multiple models by LSE from random batchs of observations.
    The best model fits for the greatest number of observations and allows to get ride off outliers.

    :param t: list abscisse values of observations
    :type t: float[]
    :param l: list of observations, should have same size as t
    :type l: float[]
    :param model: the model to be estimated from observations, setted at first with approx parameters
    :type model: Function
    """

    def __init__(self, t, l, model, *args, **kwargs):
        """
        Constructor method
        """
        self.t = t
        self.l = l
        self.model = model

    def estimate(self, consensus_fit_treshold, *args, batch_size=None, consensus_size_treshold=None, iteration_max=None,
                 LSE_iteration_max=1, **kwargs):
        """
        Runs the RANSAC estimation

        :param consensus_fit_treshold: the treshold value that defines if an observation is too far from the batch estimated model
        :type consensus_fit_treshold: float
        :param batch_size: the size of the random batch, defaults to number_of_parameters
        :type batch_size: int, optional
        :param consensus_size_treshold: treshold value of the required number of observations that agree with the batch estimation for the RANSAC to be correct, defaults to 95% of number_of_observations
        :type consensus_size_treshold: int, optional
        :param iteration_max: the maximum number of iterations for RANSAC algorithme if consensus_size_treshold is never reached, defaults to 10
        :type iteration_max: int, optional
        :param LSE_iteration_max: the maximum number of iterations for batch LSE, defaults to 1
        :type LSE_iteration_max: int, optional
        """
        self.consensus_fit_treshold = consensus_fit_treshold

        if batch_size != None:
            self.batch_size = batch_size
        else:
            self.batch_size = len(self.model.parameters)  # minimum number of observations for estimation

        if consensus_size_treshold != None:
            self.consensus_size_treshold = consensus_size_treshold
        else:
            self.consensus_size_treshold = int(len(self.t) * 0.95)  # TODO find better way to set this

        if iteration_max != None:
            self.iteration_max = iteration_max
        else:
            self.iteration_max = 10  # TODO find better way to set this

        self.consensus_points = []  # initialize to enter while loop
        self.k = 0  # loop counter
        while len(self.consensus_points) < self.consensus_size_treshold and self.k <= self.iteration_max:

            # estimation with a batch
            batch_t, batch_l = self.setBatch()
            batch_estimation = LeastSquares(batch_t, batch_l, self.model)
            batch_estimation.estimate(iteration_max=LSE_iteration_max)

            # consensual points with this estimation
            batch_consensus_points = self.determineConsensusPoints(batch_estimation.model)
            if len(batch_consensus_points) > len(self.consensus_points):
                self.consensus_points = batch_consensus_points

            self.k += 1

        # when loop's over, we take the largest number of consensual points
        # and we make an estimation with them
        ransacT = []
        ransacL = []
        for indice in self.consensus_points:
            ransacT.append(self.t[indice])
            ransacL.append(self.l[indice])

        self.ransac_estimation = LeastSquares(ransacT, ransacL, self.model)
        self.ransac_estimation.computeIterativeLSE(1, LSE_iteration_max)
        # this estimation will be the ransac estimation
        self.model = self.ransac_estimation.model

    def setBatch(self):
        """
        Creates a random batch of observations

        :return: the batch observation abscisses
        :rtype: float[]
        :return: the batch observation values
        :rtype: float[]
        """
        batch_indices = []
        batch_t = []
        batch_l = []
        for i in range(self.batch_size):
            indice = -1  # to enter while loop
            while indice in batch_indices and indice < 0:  # make sure to have different observations
                indice = randint(0, len(self.t) - 1)
            batch_indices.append(indice)
            batch_t.append(self.t[indice])
            batch_l.append(self.l[indice])

        return batch_t, batch_l

    def determineConsensusPoints(self, model):
        """
        Selects the observations that fits in the consensus_fit_treshold limits with input model

        :param model: the model to fit observations with
        :type model: Function

        :return: list of consensual observations indices
        :rtype: int[]
        """
        consensus_point_indices = []
        for i in range(len(self.t)):
            diff = abs(model.function(self.t[i]) - self.l[i])
            if diff < self.consensus_fit_treshold:
                consensus_point_indices.append(i)

        return consensus_point_indices

    def summarize(self):
        """
        Prints a summary of the RANSAC estimation
        """
        print("******************************************")
        print("*********** RANSAC ESTIMATION ************")
        print("***************  SUMMARY  ****************")
        print("******************************************")
        print()

        print('Model used : ', self.model.name)
        print('Number of iterations : ', self.k)
        print('Maximum number of iterations : ', self.iteration_max)
        print('Size of batch : ', self.batch_size)
        print()
        print('Percentage of observations fitting the estimated model : ',
              100 * len(self.consensus_points) / len(self.l))
        print('Fit divergence treshold : ', self.consensus_fit_treshold)
        print('Aimed percentage : ', 100 * self.consensus_size_treshold / len(self.l))
        print('\n')

        print("   ####### Estimated Parameters #######   ")
        for param in self.model.parameters:
            print('----------------------')
            print(param)
        print('\n')
