#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:mod:`model` 
=====================

This module contains several classes that instanciate basic functions to model raw data.
Those basic models can then be combined to create a brand new one.
Feel free to add any missing basic model. 

:Authors:
    Edgar LENHOF

:Version: 1.0 of 2020/03/06


Class overview
--------------

.. autosummary::

   Parameter
   Function
   Sinus
   Cosinus
   Linear
   Polynom
   Exponential
      
"""

import copy
import math as m
import types

from pyestimation.constants import PARAMETER_TYPES
from pyestimation.utils import overrides, addMotherClassDoc, add_method


class Parameter:
    """
    Class Parameters represents a parameter of an estimation model

    :param type: the type of your attribute reference the mathematical/physical sense of it
    :type type: str in PARAMETER_TYPES enumeration
    :param value: the value of your attribute
    :type value: float
    :param name: the name of your attribute, defaults to 'No name'
    :type name: str, optional

    :raises TypeError:  Invalid parameter type, not in PARAMETER_TYPES

    """

    def __init__(self, typeParam, value, *args, name="No name", **kwargs):
        """
        Constructor method
        """
        self.value = value
        self.setType(typeParam)
        self.name = name

    def __str__(self):
        """
        Overrides the __str__() to changes message to be printed

        :return: message that will be displayed when invoking print() method with Parameter object
        :rtype: str
        """
        string = "Parameter : " + self.name + "\n" + "Type : " + self.type + "\n" + "Value : " + str(self.value)
        return string

    # SETTERS
    def setType(self, typeParam):
        """
        Sets the type of the parameter with a type listed in PARAMETER_TYPES

        :param type: the type of your attribute reference the mathematical/physical sense of it
        :type type: str

        :raises TypeError:  Invalid parameter type, not in PARAMETER_TYPES
        """
        if typeParam in PARAMETER_TYPES:
            self.type = typeParam
        else:
            print("Entered type : ", typeParam)
            raise TypeError('''Invalid parameter type 
Please enter a type present in PARAMETER_TYPES''')

    def setValue(self, value):
        """
        Sets the value of the parameter

        :param value: the value you want to sets the argument with
        :type value: float
        """
        self.value = value

    def setName(self, name):
        """
        Sets the name of the parameter

        :param name: the name you want to sets the argument with
        :type name: str
        """
        self.name = name


class Function:
    """
    Class Function represents a mathematical function which depends on parameters
    It implements function(t) and partialDerivative(t, parameter) methods to render value on the fly
    By default, a Function object is implemented to null function

    :param name: the name of your function, defaults to 'No name'
    :type name: str, optional

    ..todo: Add the possibiity to tune this class to allow user to make their own functions
    """

    def __init__(self, *args, name="No name", **kwargs):
        """
        Constructor method
        """

        # Optional Arguments
        self.name = name
        self.parameters = []

    def function(self, x, *args, **kwargs):
        """
        Returns the value of the function for an abscisse x value

        :param x: the abscisse value
        :type x: float

        :return: value of f(x)
        :rtype: float
        """
        return 0

    def partialDerivative(self, x, param, *args, **kwargs):
        """
        Returns the value of the partial derivative from input parameter for an abscisse x value

        :param x: the abscisse value
        :type x: float
        :param param: the parameters to derived with
        :type param: Parameter

        :return: value of f(x)
        :rtype: float
        """
        return 0

    def add(self, func2):
        """
        Addition of a model function
        Changes the model function and partial derivatives
        example : new_f = old_f + another_f and new_f' = old_f' + another_f'

        :param func2: the function to be added
        :type func2: Function

        :return: the Function object added by func2
        :rtype: Function
        """

        func1 = copy.deepcopy(self)  # copy of the model Function
        func2 = copy.deepcopy(func2)
        self.appendParameters(func2.parameters)  # appending parameters of func2

        #### changing the function method ####
        @add_method(Function)  # creating new method on the fly
        def new_f(self, t):
            return func1.function(t) + func2.function(t)

        self.function = types.MethodType(new_f, self)  # monkey patching the method

        #### changing the derivative method ####
        @add_method(Function)  # creating new method on the fly
        def new_f_pd(self, t, param):
            return func1.partialDerivative(t, param) + func2.partialDerivative(t, param)

        self.partialDerivative = types.MethodType(new_f_pd, self)  # monkey patching the method

        return self

    def substract(self, func2):
        """
        Substraction of a model function
        Changes the model function and partial derivatives
        example : new_f = old_f - another_f and new_f' = old_f' - another_f'

        :param func2: the function to extract
        :type func2: Function

        :return: the Function object substracted by func2
        :rtype: Function
        """

        func1 = copy.deepcopy(self)  # copy of the model Function
        func2 = copy.deepcopy(func2)
        self.appendParameters(func2.parameters)  # appending parameters of func2

        #### changing the function method ####
        @add_method(Function)  # creating new method on the fly
        def new_f(self, t):
            return func1.function(t) - func2.function(t)

        self.function = types.MethodType(new_f, self)  # monkey patching the method

        #### changing the derivative method ####
        @add_method(Function)  # creating new method on the fly
        def new_f_pd(self, t, param):
            return func1.partialDerivative(t, param) - func2.partialDerivative(t, param)

        self.partialDerivative = types.MethodType(new_f_pd, self)  # monkey patching the method

        return self

    def multBy(self, func2):
        """
        Multiplication of a model function
        Changes the model function and partial derivatives
        example : new_f = old_f * another_f and new_f' = old_f' * another_f + old_f * another_f'

        :param func2: the function to multiply by
        :type func2: Function

        :return: the Function object multiplied by func2
        :rtype: Function
        """

        func1 = copy.deepcopy(self)  # copy of the model Function
        func2 = copy.deepcopy(func2)
        self.appendParameters(func2.parameters)  # appending parameters of func2

        #### changing the function method ####
        @add_method(Function)  # creating new method on the fly
        def new_f(self, t):
            return func1.function(t) * func2.function(t)

        #### changing the derivative method ####
        @add_method(Function)  # creating new method on the fly
        def new_f_pd(self, t, param):
            return func1.partialDerivative(t, param) * func2.function(t) + func1.function(t) * func2.partialDerivative(
                t, param)

        self.partialDerivative = types.MethodType(new_f_pd, self)  # monkey patching the method
        self.function = types.MethodType(new_f, self)  # monkey patching the method

        return self

    def divideBy(self, func2):
        """
        Divison of a model function
        Changes the model function and partial derivatives
        example : new_f = old_f / another_f and new_f' = (old_f' * another_f - old_f * another_f')/another_f**2

        :param func2: the function to divide by
        :type func2: Function

        :return: the Function object divided by func2
        :rtype: Function
        """

        func1 = copy.deepcopy(self)  # copy of the model Function
        func2 = copy.deepcopy(func2)
        self.appendParameters(func2.parameters)  # appending parameters of func2

        #### changing the function method ####
        @add_method(Function)  # creating new method on the fly
        def new_f(self, t):
            return func1.function(t) / func2.function(t)

        #### changing the derivative method ####
        @add_method(Function)  # creating new method on the fly
        def new_f_pd(self, t, param):
            return (func1.partialDerivative(t, param) * func2.function(t) - func1.function(t) * func2.partialDerivative(
                t, param)) / (func2.function(t) ** 2)

        self.function = types.MethodType(new_f, self)  # monkey patching the method
        self.partialDerivative = types.MethodType(new_f_pd, self)  # monkey patching the method

        return self

    def compose(self, func2):
        """
        Composition of a model function by another
        Changes the model function and partial derivatives
        example : new_f = old_f(another_f) and new_f' = (old_f'(another_f))*another_f'

        :param func2: the function to composed by
        :type func2: Function

        :return: the Function object composed by func2
        :rtype: Function
        """

        func1 = copy.deepcopy(self)  # copy of the model Function
        func2 = copy.deepcopy(func2)
        self.appendParameters(func2.parameters)  # appending parameters of func2

        #### changing the function method ####
        @add_method(Function)  # creating new method on the fly
        def new_f(self, t):
            return func1.function(func2.function(t))

        #### changing the derivative method ####
        @add_method(Function)  # creating new method on the fly
        def new_f_pd(self, t, param):
            return (func1.partialDerivative(func2.function(t), param)) * func2.partialDerivative(t, param)

        self.function = types.MethodType(new_f, self)  # monkey patching the method
        self.partialDerivative = types.MethodType(new_f_pd, self)  # monkey patching the method

        return self

    def appendParameters(self, new_parameters):
        """
        Appends parameters to the list of parameters of the function

        :param parameters: a list of parameters to be added
        :type func2: Parameter[]
        """
        for new_param in new_parameters:
            if new_param not in self.parameters:
                self.parameters.append(new_param)

    # Operators Overloading
    def __iadd__(self, func2):

        """
        Overloads the '+=' operator
        """
        return self.add(func2)

    def __add__(self, func2):
        """
        Overloads the '+' operator

        :return: the result of the addition of the two functions
        :rtype: Function
        """
        new = Function()  # to avoid overriding the Function object
        new.add(self)
        new.add(func2)
        return new

    def __isub__(self, func2):
        """
        Overloads the '-=' operator
        """
        return self.substract(func2)

    def __sub__(self, func2):
        """
        Overloads the '-' operator

        :return: the result of the substraction of the two functions
        :rtype: Function
        """
        new = Function()  # to avoid overriding the Function object
        new.add(self)
        new.substract(func2)
        return new

    def __imul__(self, func2):
        """
        Overloads the '*=' operator
        """
        return self.multBy(func2)

    def __mul__(self, func2):
        """
        Overloads the '*' operator

        :return: the result of the multiplication of the two functions
        :rtype: Function
        """
        new = Function()  # to avoid overriding the func1 Function object
        new.add(self)
        new.multBy(func2)
        return new

    def __idiv__(self, func2):
        """
        Overloads the '/=' operator
        """
        return self.add(func2)

    def __truediv__(self, func2):
        """
        Overloads the '/' operator

        :return: the result of the division of the two functions
        :rtype: Function
        """
        new = Function()  # to avoid overriding the Function object
        new.add(self)
        new.divideBy(func2)
        return new


#    @TODO
#    def addParameter(self, parameter):
#        """
#        :todo: Add the possibiity to tune this class to allow user to make their own functions
#        """
#        pass

# SETTERS

#    @TODO
#    def setFunctionMethod(self, method):
#        """
#        :todo: Add the possibiity to tune this class to allow user to make their own functions
#        """
#        self.function = types.MethodType(method, self) #monkey patching the method
#    @TODO
#    def setPartialDerivativeMethod(self, method):
#        """
#        :todo: Add the possibiity to tune this class to allow user to make their own functions
#        """
#        self.partialDerivative = types.MethodType(method, self) #monkey patching the method
#

#    @TODO
#    def setParameters(self):
#        """
#        :todo: Add the possibiity to tune this class to allow user to make their own functions
#        """
#        pass


class Sinus(Function):
    """
    Class Sinus represents a sinusoidal function such as

    :math: `f(x) = A * \sin (\omega *x + \phi)`

    :param pulse: the pulsation of the sinusoid
    :type pulse: Parameter
    :param amplitude: the amplitude of the sinusoid, defaults to 1
    :type amplitude: Parameter, optional
    :param phase_shift: the value of sin(0), defaults to 0
    :type phase_shift: Parameter, optional

    Inherits from :
    """
    __doc__ += Function.__doc__  # append Function docstrings to this docstrings

    @overrides(Function)
    def __init__(self, pulse, *args, name='No name', amplitude=None, phase_shift=None, **kwargs):

        super().__init__(self, *args, name=name, **kwargs)

        # Mandatory Arguments
        self.pulse = pulse
        self.parameters.append(self.pulse)  # saving parameters in a list

        # Optional Arguments
        if amplitude == None:
            self.amplitude = Parameter("sinA", 1, 'one')
        else:
            self.amplitude = amplitude
            self.parameters.append(self.amplitude)
        if phase_shift == None:  # dirty trick to ensure this parameter has a value
            # but is not considered as a parameter to avoid over analyzis
            self.phase_shift = Parameter("sinPHI", 0, 'null')
        else:
            self.phase_shift = phase_shift
            self.parameters.append(self.phase_shift)

    @addMotherClassDoc(Function)
    @overrides(Function)
    def function(self, x, *args, **kwargs):
        """
        Returns the value of sin(x) = A*sin(w*x + phi)
        """

        super().function(self, x, *args, **kwargs)
        return self.amplitude.value * m.sin(self.pulse.value * x + self.phase_shift.value)

    @addMotherClassDoc(Function)
    @overrides(Function)
    def partialDerivative(self, x, param):
        """
        Returns the value of sin(x) depending the parameter
        df/dA = sin(x*w + phi)
        df/dw = x*A*cos(x*w + phi)
        df/dphi = A*cos(x*w + phi)
        """

        if param in self.parameters:  # if the parameter to derive with is a function parameter

            if param.type == 'sinA':
                return m.sin(self.pulse.value * x + self.phase_shift.value)
            elif param.type == 'sinW':
                return self.amplitude.value * x * m.cos(self.pulse.value * x + self.phase_shift.value)
            elif param.type == 'sinPHI':
                return self.amplitude.value * m.cos(self.pulse.value * x + self.phase_shift.value)
            else:
                pass

        else:  # if the function is not defined by this parameter
            pass
        return 0  # it returns 0


class Cosinus(Sinus):
    """
    Class Cosinus represents a sinusoidal function such as

    :math: `f(x) = A * \cos (\omega *x + \phi)`

    :param pulse: the pulsation of the sinusoid
    :type pulse: Parameter
    :param amplitude: the amplitude of the sinusoid, defaults to 1
    :type amplitude: Parameter, optional
    :param phase_shift: the value of sin(0), defaults to 0
    :type phase_shift: Parameter, optional

    Inherits from :
    """
    __doc__ += Function.__doc__  # append Function docstrings to this docstrings

    @overrides(Function)
    def __init__(self, pulse, *args, name='No name', amplitude=None, phase_shift=None, **kwargs):
        # basically a cosinus is a sinus shifted by -pi/2
        super().__init__(self, pulse, *args, name=name, amplitude=amplitude, phase_shift=phase_shift, **kwargs)
        self.phase_shift.setValue(self.phase_shift.value - m.pi / 2)


class Linear(Function):
    """
    Class Linear represents a linear function such as :

    :math: `f(x) = a*x + b`

    :param coefficient: the coefficient of the function, defaults to 0
    :type coefficient: Parameter, optional
    :param origin: the value of f(0), defaults to 0
    :type origin: Parameter, optional

    Inherits from :
    """
    __doc__ += Function.__doc__  # append Function docstings to this docstrings

    @overrides(Function)
    def __init__(self, *args, coefficient=None, origin=None, name='No name', **kwargs):

        super().__init__(self, *args, name=name, **kwargs)

        # Optional Arguments
        if coefficient == None:
            self.coefficient = Parameter("linearCOEFF", 0, 'null')
        else:
            self.coefficient = coefficient
            self.parameters.append(self.coefficient)

        if origin == None:
            self.amplitude = Parameter("linearORIGIN", 0, 'null')
        else:
            self.origin = origin
            self.parameters.append(self.origin)

    @addMotherClassDoc(Function)
    @overrides(Function)
    def function(self, x, *args, **kwargs):
        """
        Returns the value of f(x) = a*x + b
        """
        return self.coefficient.value * x + self.origin.value

    @addMotherClassDoc(Function)
    @overrides(Function)
    def partialDerivative(self, x, param):
        """
        Returns the value of f(x) depending on the input parameter
        df/da = x
        df/db = 1
        """
        if param in self.parameters:  # if the parameter to derive with is a function parameter

            if param.type == 'linearCOEFF':
                return x
            elif param.type == 'linearORIGIN':
                return 1
            else:
                pass

        else:  # if the function is not defined by this parameter
            pass
        return 0  # it returns 0


class Polynom(Function):
    """
    Class Polynom represents a polynomial function such as :

    :math: `f(x) = \\sum_{k=0}^{N} a_k x^k`

    :param degree: the degree of the polynom
    :type degree: float
    :param coefficients: the coefficient of each polynomial member with degree increasingly, starting with the 0, size = degree+1
    :type coefficients: Parameter[]

    Inherits from :
    """
    __doc__ += Function.__doc__  # append Function docstings to this docstrings

    @overrides(Function)
    def __init__(self, degree, coefficients, *args, name='No name', **kwargs):

        super().__init__(self, *args, name=name, **kwargs)
        self.degree = degree
        self.coefficients = coefficients
        self.parameters = self.coefficients

    @addMotherClassDoc(Function)
    @overrides(Function)
    def function(self, x, *args, **kwargs):
        """
        Returns the value of f(x) = sum(a_n * x^n)
        """
        value = 0
        for i in range(len(self.degree) + 1):
            value += self.coefficients[i].value * m.pow(x, i)
        return value

    @addMotherClassDoc(Function)
    @overrides(Function)
    def partialDerivative(self, x, param):
        """
        Returns the value of f(x) depending on the input parameter
        df/da_n = x^n
        """
        if param in self.parameters:  # if the parameter to derive with is a function parameter

            if param.type == 'polynomCOEFF':
                indice = self.parameters.index(param)
                return m.pow(x, indice)
            else:
                pass

        else:  # if the function is not defined by this parameter
            pass
        return 0  # it returns 0


class Exponential(Function):
    """
    Class Exponential represents an exponential function such as :

    :math: `f(x) = A * exp(u*x)`

    :param incoeff: the coefficient inside the exponential, defaults to 1
    :type incoeff: Parameter, optional
    :param outcoeff: the coefficient outside the exponential, defaults to 1
    :type outcoeff: Parameter, optional

    Inherits from :
    """
    __doc__ += Function.__doc__  # append Function docstings to this docstrings

    @overrides(Function)
    def __init__(self, *args, name='No name', incoeff=None, outcoeff=None, **kwargs):

        super().__init__(self, *args, name=name, **kwargs)

        if incoeff == None:
            self.incoeff = Parameter("exponentialIN", 1, 'null')
        else:
            self.incoeff = incoeff
            self.parameters.append(self.incoeff)

        if outcoeff == None:
            self.outcoeff = Parameter("exponentialOUT", 1, 'null')
        else:
            self.outcoeff = outcoeff
            self.parameters.append(self.outcoeff)

    @addMotherClassDoc(Function)
    @overrides(Function)
    def function(self, x, *args, **kwargs):
        """
        Returns the value of f(x) = A * exp(u * x)
        """
        return self.outcoeff * m.exp(self.incoeff * x)

    @addMotherClassDoc(Function)
    @overrides(Function)
    def partialDerivative(self, x, param):
        """
        Returns the value of f(x) depending on the input parameter \n
        df/dA = exp(u * x) \n
        df/du = x * A * exp(u * x)
        """

        if param in self.parameters:  # if the parameter to derive with is a function parameter

            if param.type == 'exponentialIN':
                return x * self.outcoeff * m.exp(self.incoeff * x)

            elif param.type == 'exponentialOUT':
                return m.exp(self.incoeff * x)

            else:
                pass

        else:  # if the function is not defined by this parameter
            pass
        return 0  # it returns 0
