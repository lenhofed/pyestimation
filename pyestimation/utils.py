#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
:mod:`utils` 
=====================

This module contains some usefull method that are called in the script

:Authors:
    Edgar LENHOF

:Version: 1.0 of 2020/03/06
"""

from functools import wraps
import numpy as np
import matplotlib.pyplot as plt
from pyestimation.constants import PARAMETER_TYPES, MODEL_TYPES

def fourierDecomposition(signal, *args, treshold=10, dt=1, plot=False, **kwargs):
    '''
    Returns the fourier decomposition of a signal
    
    :param signal: The signal to be decomposed
    :type signal: 1D np.array
    :param treshold: treshold percentage of maximum amplitude under which values are consider as noise, defaults to 10
    :type treshold: float, optional
    :param dt: Sample spacing, defaults to 1
    :type dt: float, optional
    :param plot: allows to plot the fourier decomposition, defaults to False
    :type plot: boolean, optional
    
    :return: frequencies
    :rtype: 1D np.array
    :return: amplitudes
    :rtype: 1D np.array
    '''    
    fourier = np.fft.fft(signal)
    amplitudes =  2*np.abs(fourier) / signal.size
    
    for i in range(amplitudes.size):
        if amplitudes[i] < np.max(amplitudes)*treshold/100: 
            amplitudes[i] = 0
            
    freq = np.fft.fftfreq(signal.size, d=dt)
    
    if plot:
        plt.figure()
        plt.plot(freq[:int(freq.size/2)], amplitudes[:int(freq.size/2)])
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Amplitude')
        plt.title('Fourier decomposition of the signal')
       
    return freq[:int(freq.size/2)], amplitudes[:int(freq.size/2)]

def findFourierPeaks(freq, amp):
    '''
    Basic method to find the frequency and amplitude of the Fourier peaks 
    
    :param freq: frequency domain of the fourier
    :type freq: 1D np.array
    :param freq: amplitude of the corresponding frequencies of the fourier
    :type freq: 1D np.array
    
    :return: list of frequency and amplitudes corresponding to peaks
    :rtype: (float,float)[]
    '''
    peaks = []
    for i in range(1,amp.size-1):
        if amp[i] > amp[i-1] and amp[i] > amp[i+1]:
            peaks.append((freq[i], amp[i]))
            
    return peaks

def add_method(cls):
    """
    Decorator that adds a method to a class during runtime

    :param cls: the Class that we want to add a method
    :type cls: Class

    :note: Taken from `add method topic`_

    .. _add method topic: https://medium.com/@mgarod/dynamically-add-a-method-to-a-class-in-python-c49204b85bd6
    """

    def decorator(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            return func(*args, **kwargs)

        setattr(cls, func.__name__, wrapper)
        # Note we are not binding func, but wrapper which accepts self but does exactly the same as func
        return func  # returning func means func can still be used normally

    return decorator


def overrides(interface_class):
    """
    Decorator that adds a method to a class during runtime

    :param interface_class: the mother Class of the method overrided
    :type cls: Class

    :note: Taken from `override topic`_
    :note: Should try to use the package 'overrides' instead, see https://github.com/mkorpela/overrides

    .. _override topic: https://stackoverflow.com/questions/1167617/in-python-how-do-i-indicate-im-overriding-a-method
    """

    def overrider(method):
        assert (method.__name__ in dir(interface_class))
        return method

    return overrider


def addParameterType(param_type):
    """
    Allows to add a parameter type to the constants.PARAMETER_TYPES which is necessary to reference a new type

    :param param_type: the new Parameter's type to be added
    :type param_type: str
    """
    PARAMETER_TYPES.append(param_type)


def printAvalaibleParameterType():
    """
    Prints the avalaible parameter types in :mod:`constants`
    """
    print("------------------------------------------")
    print("Thoose are the avalaible parameter types :")
    for param in PARAMETER_TYPES:
        print(param)
    print("------------------------------------------")

def printAvalaibleModelType():
    """
    Prints the available model types in :mod:`constants`
    """
    print("------------------------------------------")
    print("These are the available parameter types :")
    for param in MODEL_TYPES:
        print(param)
    print("------------------------------------------")


def addMotherClassDoc(cls):
    """
    Decorator that adds a method to a class during runtime

    :param cls: the Class that we want to add a method
    :type cls: Class

    :note: Adapted from `add documentation topic`_

    .. _add documentation topic: https://groups.google.com/forum/#!topic/comp.lang.python/HkB1uhDcvdk
    """

    def _fn(fn):
        if fn.__name__ in cls.__dict__:
            fn.__doc__ += "\n" + "Overrides " + fn.__name__ + " method in " + cls.__name__ + "\n" + cls.__dict__[
                fn.__name__].__doc__
        return fn

    return _fn
