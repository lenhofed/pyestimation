from setuptools import setup, find_packages

setup(
    name='pyEstimation',
    version='0.0.2',
    packages=find_packages(),
    zip_safe=False,

    install_requires=['scipy', 'matplotlib'],
    python_requires='>=3',

    # metadata to display on PyPI
    author='Edgar LENHOF',
    author_email='edgar.lenhof@tutanota.com',
    description='Python toolbox for educational purpose on numerical estimation methods.',
    long_description=open('README.md').read(),
    keywords='least squares',
    url='https://gitlab.com/lenhofed/pyestimation'
)
